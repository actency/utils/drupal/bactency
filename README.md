# Actency Starter Kit


## Index
- [Setup](#setup)
- [Using](#using)
  - [Coding standards](#coding-standards)
  - [Add a custom font](#add-a-custom-font)
  - [Add a librarie](#add-a-librarie)
  - [Add custom icons](#add-custom-icons)
  - [Wysiwyg theming (ckeditor)](#add-custom-icons)
- [Configure your environnement for theme development](#configure-your-environnement-for-theme-development)


# Setup

Bactency theme uses [Webpack](https://webpack.js.org) to compile and
bundle SASS and JS.


### 1 - Create the Sub-theme:
Copy the Bactency directory and paste it into `/themes/custom`

Rename it with the new theme name.

Example : `/themes/custom/mytheme`


### 2 - Customize Theme Name Entries:
Rename all instances of "bactency" found in the files as well as in the file names. For example, "bactency.info.yml" becomes "mytheme.info.yml".
Here is the list of files to edit:

- bactency.info.yml
- bactency.libraries.yml
- bactency.theme
- composer.json
- package.json

### 3 - Adapt info.yml
Adapt the .info.yml. You can rearrange the regions according to your need. Base theme is set to false to not depend on another theme and these future updates. This decision is taken to avoid any risk of regression and make the theming of your site as stable and personalized as possible.

### 4 - Install Dependencies and Perform Initial Compilation:
Navigate to the root of the sub-theme.
```bash
yarn
yarn dev
```
The compilation should start in "watch" mode.

Exit watch mode with ctrl+c.

### 4 - Enable the Sub-theme in Drupal:
Go to the Backend > Appearance.
Set the new sub-theme as the default.
Clear the caches

# Using

This theme is using Sass, which means that no custom css should be added in .css files.

Partials stylesheets are located in `src/scss` directory.

Run `yarn dev` to compile styles and start the watcher.

### Coding standards
We use [Stylelint](https://stylelint.io/) to lint our Sass files and [ESLint](https://eslint.org/) to lint our JS files. The project is configured to use the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript). The errors are displayed in the console when you run the webpack and can be fixed automatically by running `yarn stylelint-fix` or `yarn eslint-fix`.

### Add a custom font
Please refer to the [README](/assets/fonts/README.md) in `assets/fonts`.

### Add a libraries
To add a custom librarie :
- add your custom librarie in `yourname.libraries.yml`
- call your library where you need it (attach it in a template or call it directly in `yourname.info.yml` if you need it everywhere)

External libraries are managed with `yarn` (Avoid using CDNs as much as possible
).

### CSS Load optimization and css structure
There is no longer a `style.scss` file containing all of the css. The css files must be cut into "partial" and called if necessary in page templates in order to optimize their loading and their caching. So, each content type is a template, and each template must have its corresponding scss sheet in the `partials/paragraphs` folder. then you must declare a new library and call this basic css file in the template branch file.

 Example on a News type page, we have a hero A, 3 paragraphs (a, b and c) available out of 20 in total.
In `scss/templates/news.scss` we are going to make taxes from the sass files of paragraphs a b and c present in the `partials/paragraphs` folder. Paragraph files can also use scss `field` file imports. for example in paragraph A, we use a title and long description field already developed previously for another paragraph. It will then be necessary to use the `title.scss` and `long_description.scss` files present in the `/partials/fields/` folder to refactor what has already been done.

Then, set a library for your template in the `.libraries.yml` file containing the corresponding scss sheet in the `/scss/templates` folder. Then inserted this library into the twig template


### JS Libraries
Here is a list of JS libaries commonly used by Actency :

- [SwiperJS](https://swiperjs.com/)
- [AnimeJS](https://animejs.com/)
- [EkkoLightbox](https://ashleydw.github.io/lightbox/)
- [Masonery.js](https://masonry.desandro.com/)
- [Slick](https://kenwheeler.github.io/slick/)
- [Moment.js](https://momentjs.com/)
- [AOS](https://michalsnik.github.io/aos/)
- [Chart.js](https://www.chartjs.org/)
- [Hammer.js](https://hammerjs.github.io/).

### Add custom icons
It is recommended to use inline SVGs as much as possible by including them in the Twig template. To do this, please place your SVGs in the `/icons` folder. Ensure that the SVGs are formatted consistently and in a way that allows you to change their color using a CSS rule like:
```css
svg {
  path {
    fill: color;
  }
}
```

### Wysiwyg theming (ckeditor)
If you need to add style to wysiwyg field, add your custom style in `ckeditor.scss` file.
A FAIRE


## Configure your environnement for theme development

If the site was build with the latest technical starterkit, please refer to [Guide d'utilisation - Socle Technique 2.0](https://docs.google.com/document/d/1YjEgJXTcLO_0lIvBcNCGPJqaWCq3_tQSlZwtESOHdUQ/edit?usp=sharing)

You just have to launch `task app-toggle-dev-prod-mode` to toggle dev mode or prod mode.
This task will create all the settings file you need to desaggregate your CSS, JS, enabled twig debug, and configure caches for more efficience work.

If you have to create a new theme on a older technical starterkit, refer to the following process :

**Disable render cachin and CSS/JS aggregation**

 In your `settings.local.php` file located in `/config/drupal` :

- Check that CSS and JS aggregation are disabled :
```
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
```

- Uncomment the following line to bypass the Render API cache :
```
$settings['cache']['bins']['render'] = 'cache.backend.null';
```

**Enabled Twig debugging options**

In your `services.yml` file located in `/web/sites/default` :

Edit the following variables under the `twig.config` section :
```yaml
debug: true
auto_reload: true
cache: false
```

For twig debugging, you can use `{{ dump() }}` in twig templates or `{{ kint() }}` if you enable Kint module (provided by Devel).

## Contact information
If you need any help, have any questions or suggestions to make about this starter kit contact Geoffrey Straehli at Actency.
