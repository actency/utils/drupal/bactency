const index = () => {
  // CODE HERE
};

document.addEventListener("DOMContentLoaded", () => {
  index();
});


// OR USE A DRUPAL FUNCTION
(function ($, Drupal) {
  Drupal.behaviors.myFunction = {
    attach: function (context, settings) {
      // CODE HERE
    }
  };
})(Drupal);